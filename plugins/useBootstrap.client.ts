import { Accordion } from "bootstrap/dist/js/bootstrap.bundle";

export default defineNuxtPlugin(() => ({
    provide: {
        bootstrap: {
            Accordion,
        },
    },
}));
