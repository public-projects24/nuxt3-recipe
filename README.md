# Nuxt 3 Recipe

Recipe search application using the edamam.com API.

## Setup

1. Make sure to install the dependencies:

```bash

npm install
```

2. Rename the .env-example file to .env

3. Modify the .env file by adding your edamam.com (Recipe Search) credentials.

## Development Server

Start the development server on `http://localhost:3000`:

```bash

npm run dev

```

## Production

Build the application for production:

```bash

npm run build

```
