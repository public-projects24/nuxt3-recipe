declare global {
    type RecipesData = {
        count: number;
        from: number;
        hits: Recipes[];
        to: number;
        _links: object;
    };

    type Recipes = {
        recipe: Recipe;
        _links: object;
    };

    type Recipe = {
        image: string;
        label: string;
        totalTime: number;
        uri: string;
        calories: number;
        ingredients: object[];
        source: string;
        images: object;
    };
}

export {};
