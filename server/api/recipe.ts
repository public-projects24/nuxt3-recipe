import { useOFetch } from "~/composables/useOFetch";

export default defineEventHandler(async (event) => {
    const { uri } = getQuery(event);

    return await useOFetch(
        "/by-uri",
        {
            query: {
                type: "public",
                uri: `http://www.edamam.com/ontologies/edamam.owl#${uri}`,
            },
        },
        true
    );
});

/* const useApiFetch = (request: string, opts?: object, auth?: boolean) => {
    const config = useRuntimeConfig();
    const baseUrl = `${config.public.apiBaseUrl}/${config.public.apiVersion}`;
    const authInfo = `?app_id=${config.public.apiId}&app_key=${config.public.apiKey}&`;
    let baseRequest = "";

    request = `${baseRequest}${request}`;

    if (auth) request += authInfo;

    return $fetch(request, {
        baseURL: baseUrl,
        ...opts,
    });
}; */
