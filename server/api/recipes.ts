import { useOFetch } from "~/composables/useOFetch";

export default defineEventHandler(async (event) => {
    const { mealType, cuisineType, q, time, diet, ingr } = getQuery(event);

    try {
        return await useOFetch(
            "",
            {
                query: {
                    type: "public",
                    q: q,
                    mealType: mealType,
                    cuisineType: cuisineType,
                    time: time,
                    diet: diet,
                    ingr: ingr,
                },
            },
            true
        );
    } catch (error) {
        return { error: error };
    }
});
