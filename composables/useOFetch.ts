export const useOFetch = (request: string, opts?: object, auth?: boolean) => {
    const config = useRuntimeConfig();
    const baseUrl = `${config.apiBaseUrl}/${config.apiVersion}`;
    const authInfo = `?app_id=${config.apiId}&app_key=${config.apiKey}&`;
    let baseRequest = "";

    request = `${baseRequest}${request}`;
    if (auth) request += authInfo;

    return $fetch(request, {
        baseURL: baseUrl,
        ...opts,
    });
};
