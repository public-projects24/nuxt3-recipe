import { useFetch, useRuntimeConfig } from "#imports";
//import $fetch from "#imports";

export const useApiFetch = (request: string, opts?: object, auth?: boolean) => {
    const config = useRuntimeConfig();
    const baseUrl = `${config.apiBaseUrl}/${config.apiVersion}`;
    const authInfo = `?app_id=${config.apiId}&app_key=${config.apiKey}&`;
    let baseRequest = "";

    request = `${baseRequest}${request}`;
    if (auth) request += authInfo;

    /*  https://api.edamam.com/api/recipes/v2?type=public&app_id=4e74f069&app_key=4979eec3a4d2f24610365ae544c48faa&mealType=Breakfast&field=label&field=image&field=images&field=totalTime&field=externalId */

    return useFetch(request, {
        baseURL: baseUrl,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        ...opts,
    });
};

export const useOFetch = (request: string, opts?: object, auth?: boolean) => {
    const config = useRuntimeConfig();
    const baseUrl = `${config.apiBaseUrl}/${config.apiVersion}`;
    const authInfo = `?app_id=${config.apiId}&app_key=${config.apiKey}&`;
    let baseRequest = "";

    request = `${baseRequest}${request}`;
    if (auth) request += authInfo;

    /*  https://api.edamam.com/api/recipes/v2?type=public&app_id=4e74f069&app_key=4979eec3a4d2f24610365ae544c48faa&mealType=Breakfast&field=label&field=image&field=images&field=totalTime&field=externalId */

    return $fetch(request, {
        baseURL: baseUrl,
        ...opts,
    });
};
