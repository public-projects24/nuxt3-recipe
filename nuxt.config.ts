// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    devtools: { enabled: true },

    vite: {
        css: {
            preprocessorOptions: {
                scss: {
                    // additionalData: '@use "@/assets/_variables.scss" as *;',
                    additionalData: `
                @import "@/assets/scss/settings/variables.scss";
                @import "@/assets/scss/tools/functions.scss";
                
                `,
                },
            },
        },
        //plugins: [eslintPlugin()],
    },
    css: ["@/assets/scss/app.scss", "bootstrap-icons/font/bootstrap-icons.css"],
    modules: ["nuxt-lazy-load"],
    lazyLoad: {
        defaultImage: "/home/img-placeholder.jpg",
    },
    runtimeConfig: {
        apiBaseUrl: process.env.API_HOST_URL,
        apiVersion: process.env.API_VERSION,
        apiId: process.env.API_ID,
        apiKey: process.env.API_KEY,
    },
});
